from dolfin import PETScMatrix
import algebra

def _arity_error(a, b):
    err_msg = "Wrong numer of operands; expected {}, found {}"
    return ValueError(err_msg.format(a, b))

def _size_error(a, b):
    err_msg = "Preconditioners must be square; found sizes {} and {}"
    return ValueError(err_msg.format(a, b))

class PETScPyPC(algebra.PETScPyMat):
    """This is a base class for implementing PETSc preconditioners as
    PETSc Mat objects of 'python' type. This is useful for creating
    composite preconditioners, for investigate preconditioners in
    detail, and for using python implementations of Krylov subspace
    solvers.

    """
    _default_options = {}
    def __init__(self, *ops, **kwargs):
        self._options = self._default_options.copy()
        self._options.update(kwargs)
        if not len(ops) == 1: 
            raise _arity_error(len(ops), 1)

        op = self.ctx()(*ops, **self._options)
        self._ops = ops

        from petsc4py.PETSc import Mat        
        mat = Mat().createPython(op.sizes, context = op)
        mat.setUp()
        
        # check sizes of object
        m, n = mat.getSizes()
        if not m == n:
            raise _size_error(m, n)
        
        PETScMatrix.__init__(self, mat)

    def collapse(self):
        if hasattr(self.mat().getPythonContext(), "collapse"):
            return PETScMatrix(self.mat().getPythonContext().collapse())
        else:
            err_msg = "Matrix representation not available for type {}"
            raise NotImplementedError(err_msg.format(self.__class__.__name__))

    def transpose(self):
        return self.__class__(self._ops[0].transpose(), **self._options)

    def list_options(self):
        print self._options


    class _PETSCPYPC:
        def __init__(self, *ops, **options):
            from util import get_mat, collapse, set_from_options
            from petsc4py.PETSc import PC, COMM_WORLD

            a = get_mat(collapse(ops[0]))

            self.pc = PC().create()
            self.pc.setOperators(a)
            set_from_options(self.pc, **options)
            self.pc.setUp()

            self.sizes = a.getSizes()

        def mult(self, mat, x, y):
            self.pc.apply(x, y)

        
class PETScPyLU(PETScPyPC):
    _default_options = {"pc_type": "lu",
                "pc_factor_mat_solver_package": "superlu_dist"}

    class _PETSCPYLU(PETScPyPC._PETSCPYPC):
        def collapse(self):
            from util import diag_mat

            f = self.pc.getFactorMatrix()
            i = diag_mat(f.getSizes(), dense = True)
            x = i.duplicate()

            f.matSolve(i, x)
            return x

class PETScPyILU(PETScPyPC):
    _default_options = {"pc_type": "ilu",
                "pc_factor_mat_solver_package": "petsc"}

    class _PETSCPYILU(PETScPyPC._PETSCPYPC):
        def collapse(self):
            from util import diag_mat

            f = self.pc.getFactorMatrix()
            i = diag_mat(f.getSizes(), dense = True)
            x = i.duplicate()

            f.matSolve(i, x)
            return x


class PETScPyAMG(PETScPyPC):
    _default_options = {"pc_type": "hypre",
                        "pc_hypre_type": "boomeramg"}

    class _PETSCPYAMG(PETScPyPC._PETSCPYPC):
        pass


class PETScPyML(PETScPyPC):
    _default_options = {"pc_type": "ml"}

    class _PETSCPYML(PETScPyPC._PETSCPYPC):
        pass


class PETScPyJacobi(PETScPyPC):
    _default_options = {"pc_type"       : "jacobi",
                        "pc_jacobi_type": "diagonal", # ("diagonal", "rowsum", "rowmax")
                        "pc_jacobi_abs" : False}

    class _PETSCPYJACOBI(PETScPyPC._PETSCPYPC):
        pass

    def collapse(self):
        from util import inv_diag
        return inv_diag(PETScMatrix(self.mat().getPythonContext().pc.getOperators()[0]),
                        diag_values = self._options["pc_jacobi_type"],
                        use_abs_value = self._options["pc_jacobi_abs"])

    def transpose(self):
        return self


class PETScPySOR(PETScPyPC):
    _default_options = {"pc_type"               : "sor",
                        "pc_sor_local_symmetric": True,
                        "pc_sor_its" : 1,
                        "pc_sor_lits": 1}

    class _PETSCPYSOR(PETScPyPC._PETSCPYPC):
        pass
