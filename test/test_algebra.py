from dolfin import *
from blockpc import *
from numpy.random import randn

mesh = UnitSquareMesh(4, 4)
V = FunctionSpace(mesh, "CG", 1)
Q = FunctionSpace(mesh, "DG", 0)
VQ = V*Q


u, p = TrialFunction(V), TrialFunction(Q)
v, q = TestFunction(V), TestFunction(Q)

A0 = assemble(inner(grad(u), grad(v)) * dx)
M0 = assemble(inner(u, v) * dx)
M1 = assemble(p * q * dx)

C0 = assemble(u * q * dx)
C1 = assemble(v * p * dx)

f = Function(V).vector()
g = Function(Q).vector()

# randomize
f[:] = randn(f.local_size())
g[:] = randn(g.local_size())

def test_scale():
    B0 = 3.0 * A0
    B1 = A0 * 2
    
    # Check that correct instances are obtained
    assert isinstance(B0, PETScPyScale)
    assert isinstance(B1, PETScPyScale)

    # test matrix-vector product
    y = B0 * f
    z = 3 * (A0 * f)
    assert (y-z).norm("linf") == 0

    # Check collapse
    B = B0.collapse()
    B.axpy(-3.0, A0, True) # note: fails if B is not matrix-type
    assert B.norm("linf") == 0

    # test array
    B_array = B0.array()
    assert abs(B_array - 3*A0.array()).max() == 0


def test_sum():
    B0 = A0 + M0
    assert isinstance(B0, PETScPySum)
    # check that incompatible dimensions are caught
    try:
        B1 = A0 + M1
        raise AssertionError
    except ValueError:
        pass

    # test matrix-vector product
    y = B0 * f
    z = A0 * f + M0 * f
    assert (y-z).norm("linf") == 0

    # test collapse
    B = B0.collapse()
    B.axpy(-1., M0, True) 
    B.axpy(-1., A0, True)
    assert B.norm("linf") < DOLFIN_EPS

    # test array
    B_array = B0.array()
    assert abs(B_array - A0.array() - M0.array()).max() < DOLFIN_EPS


def test_product():
    B0 = M1 * C0
    assert isinstance(B0, PETScPyMult)
    # check that incompatible dimensions are caught
    try:
        B1 = M0 * C0
        raise AssertionError
    except ValueError:
        pass

    # test matrix-vector product
    y = B0 * f
    z = M1 * (C0 * f)
    assert (y-z).norm("linf") == 0

    # test collapse
    B = B0.collapse()
    T = PETScMatrix(as_backend_type(M1).mat().matMult(as_backend_type(C0).mat()))
    B.axpy(-1., T, True)
    assert B.norm("linf") < DOLFIN_EPS

    # test array (only makes sense in serial)
    if mpi_comm_world().getSize() == 1:
        B_array = B0.array()
        assert abs(B_array - M1.array().dot(C0.array())).max() < DOLFIN_EPS
    
def test_transpose():
    T0 = C0.T
    assert isinstance(T0, PETScPyTranspose)
    
    # test matrix-vector product
    y = T0 * g
    z = C1 * g
    assert (y-z).norm("linf") < DOLFIN_EPS

    # test collapse
    T = T0.collapse()
    T.axpy(-1, C1, True)
    assert T.norm("linf") < DOLFIN_EPS

    # test array
    assert abs(T0.array() - C1.array()).max() < DOLFIN_EPS

    # test transpose
    assert T0.T == C0
    

# TODO:
# - Test for nests, in particular with transpose
# - Tests for use in BlockOperator
