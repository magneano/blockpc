blockpc is Python module providing a simple interface for
block-preconditioning for PETSc matrices assembled by dolfin.

blockpc requires recent (at least 3.6.3) versions of PETSc and
petsc4py. The patch Mat.patch should be applied to
petsc4py/src/PETSc/Mat.pyx.


Usage examples:


- Extract submatrices from a dolfin matrix::

   V = FunctionSpace(...)
   Q = FunctionSpace(...)
   W = V * Q

   a = ...
   L = ...

   bcs = ...

   A, b = block_system_assemble(a, L, bcs)

   A00 = A[0,0]
   A11 = A[1,1]

- Easy handling of blockvectors::

   w = Function(W)
   x = w.block_vector()

   x[0] = ...
   x[1] = ...
    

- Set up a block preconditioner::

   B = BlockOperator(W)

   B[0,0] = AMG(A[0,0])
   B[1,1] = SOR(A[1,1])

- Solve PETScKrylovSolver::

   solver = PETScKrylovSolver("minres")

   solver.set_operator(A)
   solver.set_preconditioner(B)

   solver.solve(x, b)
