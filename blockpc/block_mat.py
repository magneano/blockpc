from dolfin import GenericMatrix, PETScMatrix
__all__ = ["PETScBlockMatrix", "PETScBlockOperator"]

class GenericBlockMat(GenericMatrix):
    def set_split(self, split):
        from blockpc.fieldsplit import GenericFieldSplitCouple
        if not isinstance(split, GenericFieldSplitCouple):
            err_msg = "Not a valid splitting for matrices"
            raise TypeError(err_msg)
        self.__split = split


    def get_split(self):
        return self.__split


    def __getitem__(self, obj):
        if isinstance(obj, int):
            return self[obj, :]
        elif isinstance(obj, tuple) and len(obj) == 2:
            i, j = obj
        else:
            err_msg = "indices must be integers or slices."
            raise TypeError(err_msg)

        if type(i) == slice:
            from numpy import array
            return array([self[k, j] for k in xrange(*i.indices(self.shape[0]))])
        if type(j) == slice:
            from numpy import array
            return array([self[i, k] for k in xrange(*j.indices(self.shape[1]))])
        return self.get_split().get_submatrix(self, (i,j))


    def __setitem__(self, (i,j), val):
        self.get_split().set_submatrix(self, (i,j), val)


    @property
    def shape(self):
        s0, s1 = self.get_split()
        return (len(s0), len(s1))


class PETScBlockMatrix(GenericBlockMat, PETScMatrix):
    """Monolithic PETSc block matrix (type 'seqaij' or 'mpiaij'), with
    access to submatrices. Note that extracted submatrices are copies,
    and it's not possible to set submatrices.

    """
    def __init__(self, split0, matrix, split1 = None):
        from dolfin import as_backend_type, FunctionSpace
        from blockpc import PETScFieldSplitCouple
                    
        if isinstance(split0, PETScFieldSplitCouple):
            splitting = split0
        else:
            splitting = PETScFieldSplitCouple(split0, split1)
            
        self.set_split(splitting)

        mat = as_backend_type(matrix).mat()

        if not self.get_split().get_sizes() == mat.getSizes():
            err_msg = "Nonconforming sizes for splitting {} and matrix {}".format(self.get_split().get_sizes(),
                                                                                  mat.getSizes())
            raise ValueError(err_msg)
        PETScMatrix.__init__(self, mat)


    def __mul__(self, x):
        # make sure we return  a blockvector
        from dolfin import GenericVector, PETScVector
        if isinstance(x, GenericVector):
            y = PETScVector()
            self.init_vector(y, 0)
            
            from blockpc import PETScBlockVector
            y = PETScBlockVector(self.get_split()[0], y)
            
            self.mult(x, y)
            
            return y
        
        else:
            return PETScMatrix.__mul__(self, x)


class PETScBlockOperator(GenericBlockMat, PETScMatrix):
    """Python wrapper of a PETSc matrix of type 'matnest'. Submats can be
    any type and are stored separately. Unlike PETScBlockMatrix, this
    class allows for setting submatrices. 

    """
    def __init__(self, split0, blocks = None, split1 = None):
        from blockpc import PETScFieldSplit, PETScFieldSplitCouple
        from numpy import array, ndarray, zeros
        from util import zero_mat, diag_mat
        from dolfin import as_backend_type, GenericMatrix, PETScMatrix

        if isinstance(split0, PETScFieldSplitCouple):
            splitting = split0
        else:
            splitting = PETScFieldSplitCouple(split0, split1)

        self.set_split(splitting)
                      
        if (not isinstance(blocks, ndarray)) and blocks == None:
            # instantiate with empty blocks
            blocks = zeros(self.shape)

        else:
            # make sure we have a numpy array
            blocks = array(blocks)

        # get submats
        m, n = self.shape

        submats = zeros(self.shape, dtype = object)
        self.__blocks = zeros(self.shape, dtype = object)
        self.__blocks[:,:] = blocks[:,:]
        
        # make sure we have a valid Mat and PETScMatrix for each block
        for i in xrange(m):
            for j in xrange(n):
                if isinstance(blocks[i, j], (int, float)):
                    value = blocks[i, j]

                    # get sizes 
                    size_i = self.get_split()[0].get_block_sizes(i)
                    size_j = self.get_split()[1].get_block_sizes(j)
                    
                    if value == 0:
                        # make an empty matrix
                        submats[i, j] = zero_mat((size_i, size_j))

                    elif size_i == size_j:
                        # make a diagonal matrix
                        submats[i, j] = diag_mat((size_i, size_j))
                        if value != 1: submats[i,j] *= value
                        
                    else:
                        err_msg = "Cannot set non-square block ({}, {}) to identity.".format(i, j)
                        raise ValueError(err_msg)
                        
                    self.__blocks[i,j] = PETScMatrix(submats[i,j])

                else:
                    if not isinstance(blocks[i, j], GenericMatrix):
                        err_msg = "Block ({}, {}) is not a PETScMatrix or a scalar".format(i,j)
                        raise ValueError(err_msg)

                    submats[i,j] = as_backend_type(blocks[i,j]).mat()

        # create PETSc 'matnest' Mat
        from petsc4py.PETSc import Mat
        isrows = self.get_split()[0].get_isets()
        iscols = self.get_split()[1].get_isets()
        mat = Mat().createNest(submats, isrows = isrows, iscols = iscols)
        PETScMatrix.__init__(self, mat)

    def __getitem__(self, obj):
        # Note: Implementetation to preserve python class
        return self.__blocks.__getitem__(obj)

    def __setitem__(self, obj, val):
        # Note: Implementetation to preserve python class
        if isinstance(obj, int):
            self[obj, :] = val
        elif isinstance(obj, tuple) and len(obj) == 2:
            i, j = obj
        else:
            err_msg = "indices must be integers or slices."
            raise TypeError(err_msg)

        # handle slices
        if type(i) == slice:
            from numpy import array
            for k in xrange(*i.indices(self.shape[0])):
                self[k, j] = val[k]
            return
        if type(j) == slice:
            from numpy import array
            for k in xrange(*j.indices(self.shape[1])):
                self[i, k] = val[k]
            return
        
        # handle indices (i, j)
        from blockpc.util import is_petsc_matrix, is_scalar, zero_mat, diag_mat
        from dolfin import as_backend_type, PETScMatrix
        
        if is_scalar(val):
            if val == 0:
                mat = zero_mat(self.mat().getNestSubMat(i,j).getSizes())
                val = PETScMatrix(mat)
            else:
                sizes = sizes_i, sizes_j = self.mat().getNestSubMat(i,j).getSizes()
                if sizes_i == sizes_j:
                    mat = diag_mat(sizes)
                    val = PETScMatrix(mat)
                else:
                    err_msg = "Cannot set non-square block ({}, {}) to identity.".format(i, j)
                    raise ValueError(err_msg)

        elif is_petsc_matrix(val):
            mat = as_backend_type(val).mat()
        else:
            err_msg = "Submat must be a PETScMatrix or scalar"
            raise TypeError(err_msg)
            
        # update the petsc mat object and python array
        self.mat().setNestSubMat(i, j, mat)
        self.__blocks[i,j] = val

    def _assert_consistency(self):
        from numpy import ndindex
        from util import is_scalar, get_mat
        for (i,j) in ndindex(self.shape):
            mat_ij = PETScMatrix(self.mat().getNestSubMat(i,j))
            if is_scalar(self[i,j]):
                if self[i,j] == 0:
                    assert mat_ij.norm("linf") == 0

            else:
                if is_matrix_type(self[i,j]):
                    assert (self[i,j] - mat_ij).norm("linf") == 0
                else:
                    assert self[i,j].mat().getPythonContext() == mat_ij.mat().getPythonContext() != None
                

    def __mul__(self, other):
        # make sure we return  a blockvector
        from dolfin import GenericVector, PETScVector
        if isinstance(other, GenericVector):
            from blockpc import PETScBlockVector
            x = other
            y = PETScVector()
            self.init_vector(y, 0)

            y = PETScBlockVector(self.get_split()[0], y)
            self.mult(x, y)
            return y
        
        elif isinstance(other, PETScBlockOperator):
            A = self[:,:]
            B = other[:,:]
            C = A.dot(B)
            return PETScBlockOperator(self.get_split()[0], C, 
                                      split1 = other.get_split()[1])
            
            
        else:
            return PETScMatrix.__mul__(self, x)

    def array(self):
        from numpy import ndindex, newaxis, zeros
        array = zeros((self.size(0), self.size(1)))
        for (i,j) in ndindex(self.shape):
            rows = self.get_split()[0].dofs(i)[:, newaxis]
            cols = self.get_split()[1].dofs(j)
            array[rows, cols] = self[i,j].array()
        return array
