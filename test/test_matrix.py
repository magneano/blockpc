from dolfin import *
from blockpc import *

mesh = UnitSquareMesh(4, 4)
V = FunctionSpace(mesh, "CG", 1)
Q = FunctionSpace(mesh, "DG", 0)
VQ = V*Q

u, p = TrialFunctions(VQ)
v, q = TestFunctions(VQ)

form = inner(grad(u), grad(v)) * dx + p * q * dx

M = assemble(form)

u, p = TrialFunction(V), TrialFunction(Q)
v, q = TestFunction(V), TestFunction(Q)

form0 = inner(grad(u), grad(v)) * dx
form1 = p * q * dx

M0 = assemble(form0)
M1 = assemble(form1)


### ---- PETScBlockMatrix ----
def test_PETScBlockMatrix_init():
    A0 = PETScBlockMatrix(VQ, M)
    try:
        A1 = PETScBlockMatrix(V, M)
        raise AssertionError
    except ValueError:
        pass

    A1 = PETScBlockMatrix(VQ, M, split1 = VQ)
    A1 = PETScBlockMatrix(PETScFieldSplit(VQ), M)
    A1 = PETScBlockMatrix(PETScFieldSplitCouple(VQ, VQ), M)

def test_PETScBlockMat_getitem():
    A0 = PETScBlockMatrix(VQ, M)
    
    # check that off-diagonal blocks are zero
    assert A0[1,0].norm("linf") == 0
    assert A0[0,1].norm("linf") == 0

    # check remaining blocks
    x =  PETScBlockVector(VQ)
    y =  PETScBlockVector(VQ)
    
    from numpy.random import rand
    x[...] = rand(x.local_size())
    y[...] = M * x

    assert y.norm("linf") > 1
    assert (y[0] - A0[0,0] * x[0]).norm("linf") < DOLFIN_EPS
    assert (y[1] - A0[1,1] * x[1]).norm("linf") < DOLFIN_EPS


def test_PETScBlockMat_matvec():
    A0 = PETScBlockMatrix(VQ, M)
    
    x = PETScBlockVector(VQ)
    
    from numpy.random import rand
    x[...] = rand(x.local_size())
    
    y = A0 * x
    z = M * x
    
    mpi_comm_world().barrier()

    assert isinstance(y, PETScBlockVector)
    assert (y - z).norm("linf") == 0
    


### ---- PETScBlockOperator ----
def test_PETScBlockOperator_init():
    # without matrix
    B0 = PETScBlockOperator(VQ)
    B1 = PETScBlockOperator([V,Q])
    B2 = PETScBlockOperator([V,Q], split1 = VQ)
    B3 = PETScBlockOperator(PETScFieldSplit(VQ))
    B4 = PETScBlockOperator(PETScFieldSplit(VQ), split1 = PETScFieldSplit([V,Q]))
    B5 = PETScBlockOperator(PETScFieldSplitCouple(VQ, VQ))
    B5 = PETScBlockOperator(PETScFieldSplitCouple([V,Q], [V,Q]))
    
    # with matrix
    C0 = PETScBlockOperator([V,Q], blocks = [[M0, 0], [0, M1]])
    C1 = PETScBlockOperator([V,Q], split1 = [V,Q],  blocks = [[M0, 0], [0, M1]])

def test_PETScBlockOperator_getitem():
    C1 = PETScBlockOperator([V,Q], [[M0, 0], [0, M1]])
    assert C1[0,0] == M0 and C1[1,1] == M1
    assert C1[0,1].norm("linf") == C1[1,0].norm("linf") == 0

    from numpy import ndindex
    for i, j in ndindex((2,2)):
        assert (C1[i,j] - PETScMatrix(C1.mat().getNestSubMat(i,j))).norm("linf") == 0


def test_PETScBlockOperator_setitem():
    A0 = PETScBlockMatrix(VQ, M)
    C0 = PETScBlockOperator(VQ)
    C1 = PETScBlockOperator([V,Q])

    C1[0,0] = M0
    C1[1,1] = M1
    
    # test that getitem and setitem works
    assert C1[0,0] == M0 and C1[1,1] == M1
    
    C0[:,:] = A0[:,:]  # C0 is A0 but in matnest format

    from numpy import ndindex
    for i, j in ndindex((2,2)):
        # check that the matrices are the same
        assert (C0[i,j] - A0[i,j]).norm("linf") == 0
        # also check consistency wrt PETSc
        assert (C0[i,j] - PETScMatrix(C0.mat().getNestSubMat(i,j))).norm("linf") == 0
    
        
    # finally test matrix-vector product
    from numpy.random import rand
    x = Function(VQ).vector()
    x[:] = rand(x.local_size())
    assert (C0 * x - A0 * x).norm("linf") < DOLFIN_EPS


