"""NOTE: 
This file modifies certain attributes of PETScMatrix and add new ones.

Matrix algebra operations results in a "lazy evaluation", which is
evaluated when needed, i.e. when applied to a vector.

Call the collapse function to have the algebraic operations carried
out explicitly to form a new matrix.

"""
import dolfin
if (not dolfin.has_petsc() or not dolfin.has_petsc4py()):
    err_msg = "This module requires PETSc and petsc4py."
    raise ImportError(err_msg)

from fieldsplit import *
from block_mat import *
from block_vec import *
from algebra import *

from precond import PETScPyLU as LU
from precond import PETScPyILU as ILU
from precond import PETScPyAMG as AMG
from precond import PETScPyJacobi as Jacobi
from precond import PETScPySOR as SOR

from util import block_assemble
from util import block_assemble_system

from dolfin import PETScMatrix, GenericMatrix, Matrix, PETScKrylovSolver
from dolfin import Function
### ------------------- PETScMatrix attributes -------------------
__d__mul__  = getattr(GenericMatrix, "__mul__")
__d__rmul__ = getattr(GenericMatrix, "__rmul__")
__d__add__  = getattr(GenericMatrix, "__add__")
__d__radd__ = getattr(GenericMatrix, "__radd__")
__d__sub__  = getattr(GenericMatrix, "__sub__")
__darray    = getattr(GenericMatrix, "array")
__dnorm     = getattr(GenericMatrix, "norm")

def __mul__(self, other):
    from util import is_petsc_matrix, is_scalar
    if is_scalar(other):
        if other == 1:
            return self
        elif other == 0:
            return 0
        else:
            if is_petsc_matrix(self):
                return PETScPyScale(self, other)

    elif is_petsc_matrix(self) and is_petsc_matrix(other):
        return PETScPyMult(self, other)
        
    # fallback
    return __d__mul__(self, other)

    
def __rmul__(self, other):
    from util import is_petsc_matrix, is_scalar
    if is_scalar(other):
        if other== 1:
            return self
        elif other == 0:
            return 0
        else:
            if is_petsc_matrix(self):
                return PETScPyScale(self, other)

    elif is_petsc_matrix(self) and is_petsc_matrix(other):
        return PETScPyMult(other, self)

    # fallback
    return __d__rmul__(self, other)


def __add__(self, other):
    from util import is_petsc_matrix, is_scalar
    if is_scalar(other) and other == 0:
        return self

    elif is_petsc_matrix(self) and is_petsc_matrix(other):
        if self == other:
            return PETScPyScale(self, 2)
        else:
            return PETScPySum(self, other)

    # fallback
    return __d__add__(self, other)


def __sub__(self, other):
    from util import is_petsc_matrix, is_scalar
    if is_scalar(other) and other == 0:
        return self

    elif is_petsc_matrix(self) and is_petsc_matrix(other):
        if self == other:
            return 0
        else:
            return PETScPySum(self, -other)

    # fallback
    return __d__sub__(self, other)


def __radd__(self, other):
    from util import is_petsc_matrix, is_scalar
    if is_scalar(other) and other == 0:
        return self

    elif is_petsc_matrix(self) and is_petsc_matrix(other):
        if self == other:
            return PETScPyScale(self, 2)
        else:
            return PETScPySum(other, self)

    # fallback
    return __d__add__(self, other)

def __neg__(self):
    return self * -1


def __array(self):
    if hasattr(self, "collapse"):
        return self.collapse().array()
        
    return __darray(self)

def __norm(self, norm_type):
    if hasattr(self, "collapse"):
        return self.collapse().norm(norm_type)

    return __dnorm(self, norm_type)


def __transpose(self):
    if not isinstance(self, algebra.PETScPyMat):
        return PETScPyTranspose(self)
    else:
        return self.transpose()

def __repr__(self):
    return "{}.{} instance of size ({},{}) at {}".format(self.__class__.__module__,
                                                         self.__class__.__name__,
                                                         self.size(0),
                                                         self.size(1),
                                                         hex(id(self)))

def __str__(self):
    return "{}({},{})".format(self.__class__.__name__, self.size(0), self.size(1))


__T = property(fget = __transpose)
setattr(GenericMatrix, "__mul__", __mul__)
setattr(GenericMatrix, "__rmul__", __rmul__)
setattr(GenericMatrix, "__add__", __add__)
setattr(GenericMatrix, "__radd__", __radd__)
setattr(GenericMatrix, "__sub__", __sub__)
setattr(GenericMatrix, "__neg__", __neg__)
setattr(GenericMatrix, "array", __array)
setattr(GenericMatrix, "transpose", __transpose)
setattr(GenericMatrix, "T", __T)
setattr(Matrix, "__repr__", __repr__)
setattr(Matrix, "__str__", __str__)
setattr(PETScMatrix, "__repr__", __repr__)
setattr(PETScMatrix, "__str__", __str__)

### ---------------- PETScKrylovSolver attributes  --------------
def __set_preconditioner(self, B):
    if not isinstance(B, PETScMatrix):
        err_msg = "Requries a PETScmatrix instance"
        raise TypeError(err_msg)

    self.ksp().setOperators(A = self.ksp().getOperators()[0],
                            P = B.mat())
    self.ksp().getPC().setType("mat")
    self.ksp().getPC().setUp()

setattr(PETScKrylovSolver, "set_preconditioner", __set_preconditioner)

### ------------------- Function attributes   -------------------
def __block_vector(self):
    return PETScBlockVector(PETScFieldSplit(self.function_space()), self.vector())

setattr(Function, "block_vector", __block_vector)
