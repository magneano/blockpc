from dolfin import PETScMatrix
__all__ = ["PETScPyScale", "PETScPySum", "PETScPyMult", "PETScPyTranspose"]

def _arity_error(a, b):
    err_msg = "Wrong numer of operands; expected {}, found {}"
    return ValueError(err_msg.format(a, b))

def _size_error(a, b):
    err_msg = "Found incompatible sizes {} and {}"
    return ValueError(err_msg.format(a, b))

class PETScPyMat(PETScMatrix):
    """Wrapper for PETSc Mat objects of type 'python'. This is a base
    class and it does not have a python context.

    Subclasses should implement a python context as class with a mult
    attribute, e.g. 
    
    class MyPythonMat(PETScPyMat):
        class _MYPYTHONMAT:
            def init(self, *ops):
                # do something

            def mult(self, mat, x, y):
                # do something with x, place values in y

    Note that the name of python context is expected to be the class
    name in upper case, preceded by an underscore.

    Optionally the subclass can implement at collapse(self) attribute,
    returning a matrix type PETSc Mat object.

    """
    def __init__(self, *ops):
        op = self.ctx()(*ops)
        self._ops = ops

        from petsc4py.PETSc import Mat        
        mat = Mat().createPython(op.sizes, context = op)
        mat.setUp()
        
        PETScMatrix.__init__(self, mat)

    def ctx(self):
        return getattr(self, "_" + self.__class__.__name__.upper())

    def collapse(self):
        from util import collapse
        return PETScMatrix(self.ctx()(*map(collapse, self._ops)).collapse())

    def transpose(self):
        from util import transpose
        return self.__class__(*list(reversed(map(transpose, self._ops))))
        
    def array(self):
        return self.collapse().array()

    def norm(self, norm_type):
        return self.collapse().norm(norm_type)

    def __repr__(self):
        return  PETScMatrix.__repr__(self) +";[".format(hex(id(self))) \
                + ",".join(op.__repr__() for op in self._ops) + "]"
        
    def __str__(self):
        return self.__class__.__name__.replace("PETScPy", "")  \
               + "[" + ",".join(str(op) for op in self._ops) + "]"
    


class PETScPyScale(PETScPyMat):
    class _PETSCPYSCALE:
        def __init__(self, *ops):
            if not len(ops) == 2: 
                raise _arity_error(len(ops), 2)

            from util import get_mat
            self.a = get_mat(ops[0])
            self.s = float(ops[1])

            self.sizes = self.a.getSizes()

        def mult(self, mat, x, y):
            self.a.mult(x, y)
            y.scale(self.s)

        def collapse(self):
            b = self.a.copy()
            b.scale(self.s)
            return b


class PETScPySum(PETScPyMat):
    class _PETSCPYSUM:
        def __init__(self, *ops):
            if not len(ops) == 2: 
                raise _arity_error(len(ops), 2)

            from util import get_mat
            self.a = get_mat(ops[0])
            self.b = get_mat(ops[1])
            
            if not self.a.getSizes() == self.b.getSizes():
                raise _size_error(self.a.getSizes(), self.b.getSizes())
            self.sizes = self.a.getSizes()

        def mult(self, mat, x, y):
            z = y.duplicate()
            self.a.mult(x, y)
            self.b.mult(x, z)
            y.axpy(1, z)

        def collapse(self, nzp = 0, copy = True):
            from util import is_dense
            if is_dense(self.a) != is_dense(self.b):
                if is_dense(self.a):
                    a, b = self.a, self.b.copy()
                else:
                    a, b = self.b, self.a.copy()
                b.convert("dense")
            else:
                a, b = self.a, self.b
            return a + b
        

class PETScPyMult(PETScPyMat):
    class _PETSCPYMULT:
        def __init__(self, *ops):
            if not len(ops) == 2: 
                raise _arity_error(len(ops), 2)

            from util import get_mat
            self.a = get_mat(ops[0])
            self.b = get_mat(ops[1])
            
            if not self.a.getSizes()[1] == self.b.getSizes()[0]:
                raise _size_error(self.a.getSizes(), self.b.getSizes())
            self.sizes = (self.a.getSizes()[0], self.b.getSizes()[1])

        def mult(self, mat, x, y):
            z = self.b.createVecLeft()
            self.b.mult(x, z)
            self.a.mult(z, y)

        def collapse(self):
            return self.a * self.b

class PETScPyTranspose(PETScPyMat):
    class _PETSCPYTRANSPOSE:
        def __init__(self, *ops):
            if not len(ops) == 1: 
                raise _arity_error(len(ops), 1)

            from util import get_mat
            self.a = get_mat(ops[0])
            a_sizes = self.a.getSizes()
            self.sizes = (a_sizes[1], a_sizes[0])

        def mult(self, mat, x, y):
            self.a.multTranspose(x, y)

        def collapse(self):
            if self.sizes[0] == self.sizes[1]:
                return self.a.copy().transpose()
            else:
                from petsc4py.PETSc import Mat
                m = Mat()
                self.a.transpose(m)
                return m

    def transpose(self):
        return self._ops[0]
