from dolfin import GenericVector, PETScVector

class GenericBlockVector(GenericVector):
    def __iter__(self):
        for i in xrange(self.num_blocks()):
            yield(self[i])
            
    def set_split(self, split):
        self.__split = split

    def get_split(self):
        return self.__split

    def __getitem__(self, key):
        from dolfin import as_backend_type
        if isinstance(key, int):
            return self.__split.get_subvector(self, key)

        elif key == Ellipsis:
            return as_backend_type(self)
            
        else:
            err_msg = "Only integers, slices and ellipses are valid indices."
            raise TypeError(err_msg)

    def __setitem__(self, key, val):
        from dolfin import as_backend_type
        if isinstance(key, int):
            self.get_split().set_subvector(self, key, val)

        elif key == Ellipsis:
            as_backend_type(self).__setslice__(0, -1, val)
            
        else:
            err_msg = "Only integers, slices and ellipses are valid indices."
            raise TypeError(err_msg)
    
    
    def num_blocks(self):
        return len(self.get_split())

    def copy(self):
        from dolfin import as_backend_type
        return self.__class__(self.get_split(), as_backend_type(self).copy())
        



class PETScBlockVector(GenericBlockVector, PETScVector):
    def __init__(self, split, vector = None):
        from dolfin import as_backend_type
        from petsc4py.PETSc import Vec
        from blockpc import PETScFieldSplit
        
        if not isinstance(split, PETScFieldSplit):
            split = PETScFieldSplit(split)
        
        self.set_split(split)
        
        if not vector:
            vec = split._PETScFieldSplit__create_vec()
        
        elif isinstance(vector, Vec):
            vec = vector

        elif isinstance(vector, GenericVector):
            vec = as_backend_type(vector).vec()

        else:
            err_msg = "Unable to wrap object of type {} as {}.".format(vector.__class__, self.__class__)
            raise TypeError(err_msg)
        
        if vec.getSizes() != split.get_sizes():
            err_msg = "Vector parallel layout does not match splitting parallel layout."
            raise ValueError(err_msg)

        PETScVector.__init__(self, vec)
