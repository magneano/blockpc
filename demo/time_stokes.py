""" Solve lid-driven cavity problem for the Stokes equations

  du/dt - nu * div grad u = 0    in (0,1) x (0,1)
                        u = g    on {x[1] = 1}
                        u = 0    on {x[1] = 0} U {x[0] = 0} U {x[0] = 1}

The linear problem (for a single timestep) is solved with minres with a uniform preconditioner.
"""
from dolfin import *
from blockpc import *
from matplotlib import pyplot
parameters["linear_algebra_backend"] = "PETSc"

nu = 1e-1
dt = 1e+4


# Standard Taylor-Hood functionspace
mesh = UnitSquareMesh(32, 32)
P1 = FiniteElement("Lagrange", triangle, 1)
P2 = FiniteElement("Lagrange", triangle, 2)
TH = (P2 * P2) * P1

W = FunctionSpace(mesh, TH)

# Define the stokes equations
u, p = TrialFunctions(W)
v, q = TestFunctions(W)

f = Constant((0., 0.))

a = (Constant(nu) * inner(grad(u), grad(v)) + Constant(1./dt) * inner(u, v) 
     + p * div(v) + q * div(u)) * dx
m = (inner(u, v)  + p * q) * dx
k = m + (inner(grad(u), grad(v)) + inner(grad(p), grad(q))) * dx

L = inner(f, v) * dx

# Noslip BC on the walls, including constant velocity on lid
bcs = [DirichletBC(W.sub(0), Constant((1., 0.)), "x[1]>1-DOLFIN_EPS"),
       DirichletBC(W.sub(0), Constant((0., 0.)), "on_boundary&&(x[1]<1-DOLFIN_EPS)")]


A, b = block_assemble_system(a, L, bcs)
M = block_assemble(m)
K = block_assemble(k)


# Set the nullspace (constant pressure)
nullspace_vector = Function(W).block_vector()
nullspace_vector[1] = 1.

try:
    # old way to set nullspace, changed in dolfin dev
    nullspace = VectorSpaceBasis(nullspace_vector)
    A.set_nullspace(nullspace)
    nullspace.orthogonalize(b)

except:
    # otherwise use petsc
    from petsc4py import PETSc
    nullspace = PETSc.NullSpace().create([nullspace_vector.vec()])
    A.mat().setNearNullSpace(nullspace)
    #nullspace.remove(b.vec()) # something seems wrong with this in PETSc master


# Build preconditioner
B = PETScBlockOperator(W)
B[0, 0] = AMG(A[0,0])
B[1, 1] = nu * SOR(M[1,1]) + (1./dt) * AMG(K[1,1])


# solve
solver = PETScKrylovSolver("minres")
solver.parameters["monitor_convergence"] = True
solver.parameters["relative_tolerance"] = 1e-8

solver.set_operator(A)
solver.set_preconditioner(B)

w = Function(W)
solver.solve(w.vector(), b)


u, p = w.split()
plot(u)

if parameters["plotting_backend"] == "matplotlib":
    from matplotlib import pyplot
    pyplot.show()

else:
    interactive()
