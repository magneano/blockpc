from dolfin import *
from blockpc import *

mesh = UnitSquareMesh(7, 7)
V = FunctionSpace(mesh, "CG", 1)
Q = FunctionSpace(mesh, "DG", 0)

V3 = VectorFunctionSpace(mesh, "CG", 1, dim = 3)
VQ = MixedFunctionSpace([V, Q])

spaces = [V, Q, V3, VQ]

vectors = v,q, v3, vq \
        = [as_backend_type(Function(space).vector()) for space in spaces]

w = vq.copy()

A = assemble(inner(TrialFunction(VQ), TestFunction(VQ)) * dx)


### --- tests for PETScBlockVector ---- 
def test_PETScBlockVector_init():
    # init with vector
    v_a = PETScBlockVector(V, v)
    v3_a = PETScBlockVector(V3, v3)
    vq_a = PETScBlockVector(VQ, vq)

    # init without vector
    v_b = PETScBlockVector(V)
    v3_b = PETScBlockVector(V3)
    vq_b = PETScBlockVector(VQ)
    w_b = PETScBlockVector([V,Q])

    # check consistency by assingment
    v_a[:] = v_b[:]
    v3_a[:] = v3_b[:]
    vq_a[:] = vq_b[:]
    
def test_PETScBlockVector_getitem():
    PETScBlockVector(VQ, vq)[0]
    PETScBlockVector([V,Q])[1]

def test_PETScBlockVector_setitem():
    w0 = PETScBlockVector(VQ, vq)
    w1 = PETScBlockVector([V,Q])
    
    w0[0] = 1.
    w0[1] = 2.
    
    w1[1] = 2.
    w1[0] = 3.
    
    assert all(w0[0] == 1.)
    assert all(w0[1] == 2.)
    
    assert all(w1[0] == 3.)
    assert all(w1[1] == 2.)

    # also check arrays
    dofs_VQ0_local = VQ.sub(0).dofmap().dofs() - VQ.dofmap().index_map().local_range()[0]
    dofs_VQ1_local = VQ.sub(1).dofmap().dofs() - VQ.dofmap().index_map().local_range()[0]
   
    assert all(w0.array()[dofs_VQ0_local] == 1.)
    assert all(w0.array()[dofs_VQ1_local] == 2.)
    
    lsizeV = Function(V).vector().local_size()
    lsizeQ = Function(Q).vector().local_size()

    assert all(w1.array()[:lsizeV] == 3)
    assert all(w1.array()[lsizeV:] == 2)


def test_BlockVec_iter():
    x = PETScBlockVector(V3)
    assert len([xi for xi in x]) == x.num_blocks() == 3

    y = PETScBlockVector([V, Q])
    assert len([yi for yi in y]) == y.num_blocks() == 2


def test_BlockVec_copy():
    from numpy.random import rand

    x = PETScBlockVector(V3)
    x[...] = rand(x.local_size())
    x_copy = x.copy()
    
    
    y = PETScBlockVector([V, Q])
    y[...] = rand(y.local_size())
    y_copy = y.copy()
    
    
    # Check that object instance is correct
    assert isinstance(x_copy, PETScBlockVector)
    assert isinstance(y_copy, PETScBlockVector)
    
    # Make sure values are the same
    assert all(x == x_copy)
    assert all(y == y_copy)
    
    # Make sure the data is different
    x[0] = 0.
    y[1] = 0.
    assert not all(x == x_copy)
    assert not all(y == y_copy)


def test_Function_block_vector():
    f = Function(VQ)
    from numpy.random import rand
    f.vector().set_local(rand(f.vector().local_size()))
    w = f.block_vector()
    assert w.num_blocks() == 2
    assert all(w == f.vector())
