_matrix_free_types = ["python", "transpose"]
_dense_types = ["seqdense", "mpidense"]
_sparse_types = ["seqaij", "mpiaij"]

def is_matrix_type(obj):
    from petsc4py.PETSc import Mat
    from dolfin import as_backend_type
    if isinstance(obj, Mat):
        mat = obj
    else:
        mat = as_backend_type(obj).mat()
    return mat.getType() not in _matrix_free_types


def is_petsc_matrix(obj):
    from dolfin import PETScMatrix, as_backend_type
    return isinstance(as_backend_type(obj), PETScMatrix)

def is_scalar(obj):
    return isinstance(obj, (float, int))

    
def is_dense(obj):
    from petsc4py.PETSc import Mat
    from dolfin import as_backend_type
    if isinstance(obj, Mat):
        mat = obj
    else:
        mat = as_backend_type(obj).mat()
    return mat.getType() in _dense_types


def is_zero(obj):
    if isinstance(obj, (float, int)) and obj == 0:
        return True


def get_mat(obj):
    if not is_petsc_matrix(obj):
        err_msg = "Object is not a PETSc matrix"
        raise TypeError(err_msg)

    from dolfin import as_backend_type
    return as_backend_type(obj).mat()


def get_sizes(obj):
    return get_mat(obj).getSizes()
    

def as_petscobject(obj):
    from dolfin import PETScVector, PETScMatrix
    obj = as_backend_type(obj)
    if not isinstance(mat, (PETScVector, PETScMatrix)):
        err_msg = "Not a PETSc object"
        raise ValueError(err_msg)
    return obj
        

def collapse(obj, ensure_copy = False):
    if hasattr(obj, "collapse"):
        return obj.collapse()
    else:
        return obj


def transpose(obj):
    if hasattr(obj, "transpose"):
        return obj.transpose()
    from numpy import isscalar
    if isscalar(obj):
        return obj
    
    err_msg = "Unable to transpose object: {}".format(obj)
    raise RunTimeError(err_msg)

def zero_mat(sizes):
    from petsc4py.PETSc import Mat
    z = Mat().createAIJ(sizes, nnz = 0)
    z.setUp()
    z.assemble()
    return z

def diag_mat(sizes, dense = False):
    from petsc4py.PETSc import Mat, Vec
    from numpy import ones

    if not sizes[0] == sizes[1]:
        err_msg = "Diagonal matrix must be square"
        raise ValueError(err_msg)

    if dense:
        i = Mat().createDense(sizes)
    else:
        i = Mat().createAIJ(sizes, nnz = 1)
    i.setUp()

    d = Vec().createMPI(sizes[0])
    d += 1

    i.setDiagonal(d)
    i.assemble()

    return i

def inv_diag(matrix, diag_values = "diagonal", use_abs_value = False):
    from dolfin import PETScMatrix, as_backend_type
    matrix = collapse(matrix)
    inv_diag_matrix = PETScMatrix(as_backend_type(matrix).mat().duplicate())
    inv_diag_matrix.zero()
    
    if diag_values == "diagonal":
        diag_vector = as_backend_type(matrix).mat().getDiagonal()
    elif diag_values == "rowsum":
        diag_vector = as_backend_type(matrix).mat().getRowSum()
    elif diag_values == "rowmax":
        diag_vector = as_backend_type(matrix).mat().getRowMax()
    
    from numpy import where 
    diag_vector.array[where(diag_vector.array == 0)[0]] = 1.

    if use_abs_value:
        diag_vector.abs()

    diag_vector.reciprocal()
    as_backend_type(inv_diag_matrix).mat().setDiagonal(diag_vector)

    return inv_diag_matrix


def set_from_options(petsc_object, *argv, **kwargs):
    import petsc4py
    from petsc4py import PETSc
    
    opts, prefix = PETSc.Options(), petsc_object.getOptionsPrefix()
    
    # save existing options before clearing
    opts_dict = opts.getAll()
    for key in opts.getAll(): opts.delValue(key)
    
    # set options and set the petsc object
    for (key, val) in kwargs.iteritems():
        if type(val) == bool:
            if val == False: 
                continue
            else:
                val = ""
        opts.setValue((prefix if prefix else "") +  key, val)

    for key in argv:
        opts.setValue((prefix if prefix else "") +  key, "")
    petsc_object.setFromOptions()
    
    # restore to original state
    for key in opts.getAll():
        opts.delValue(key)
    for (key, val) in opts_dict.iteritems():
        opts.setValue(key, val)

def block_assemble(form, *argv, **kwargs):
    from dolfin import assemble, GenericMatrix, GenericVector
    from blockpc import PETScBlockMatrix, PETScBlockVector
    
    # assemble form, let dolfin determine the rank
    A = assemble(form, *argv, **kwargs)
    
    if isinstance(A, GenericMatrix):
        assert len(form.arguments()) == 2
        # wrap as block matrix
        V, W = [argument.function_space() for argument in form.arguments()]
        return PETScBlockMatrix(V, A, split1 = W)

    if isinstance(A, GenericVector):
        assert len(form.arguments()) == 1
        # wrap as block vector
        b = A
        V = form.arguments()[0].function_space()
        return PETScBlockVector(V, b)

def block_assemble_system(a_form, L_form, bcs = None, *argv, **kwargs):
    from dolfin import assemble_system, GenericMatrix, GenericVector
    from blockpc import PETScBlockMatrix, PETScBlockVector
    
    # assemble system; dolfin should ensure matching functionspaces
    A, b = assemble_system(a_form, L_form, bcs, *argv, **kwargs)
    
    V = L_form.arguments()[0].function_space()

    return PETScBlockMatrix(V, A), PETScBlockVector(V, b)        


    
