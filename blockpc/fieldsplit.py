__all__ = ["PETScFieldSplit", "PETScFieldSplitCouple"]


class GenericFieldSplit(object):
    def __init__(self, subspaces):
        from dolfin import FunctionSpace
        from numpy import array, ndarray
        
        # init from (Mixed, Vector)FunctionSpace
        if isinstance(subspaces, FunctionSpace):
            W = subspaces
            if W.num_sub_spaces() > 0:
                subspaces = [W.sub(i) for i in xrange(W.num_sub_spaces())]
            else:
                subspaces = [W]
                
            self.__dofs = [sub.dofmap().dofs() for sub in subspaces]
            
        # init from list of FunctionSpace
        elif hasattr(subspaces, "__len__") and all([isinstance(sub, FunctionSpace) for sub in subspaces]):
            # In this case we need to enforce global ordering
            from numpy import zeros, cumsum
            offs = zeros(len(subspaces), dtype = "intc")
            offs[1:] = cumsum([len(sub.dofmap().dofs()) for sub in subspaces[:-1]])
            offs -= [sub.dofmap().index_map().local_range()[0] for sub in subspaces]
            offs += sum([sub.dofmap().index_map().local_range()[0] for sub in subspaces])
            self.__dofs = [sub.dofmap().dofs() + off for (sub,off) in zip(subspaces, offs)]

        else:
            err_msg = "Unable to determine blocks from object {}".format(subspaces)
            raise TypeError(err_msg)
        
        self.__subspaces = subspaces
        
        # determine sizes
        self.__local_sizes  = [len(sub.dofmap().dofs()) for sub in subspaces]
        self.__global_sizes = [sub.dim() for sub in subspaces]


    def dofs(self, sub_id):
        return self.__dofs[sub_id]


    def get_block_sizes(self, sub_id):
        return (self.__local_sizes[sub_id], self.__global_sizes[sub_id])


    def get_sizes(self):
        return (sum(self.__local_sizes), sum(self.__global_sizes))


    def __len__(self):
        return len(self.__subspaces)


    def create_vector(self):
        # Backend dependent
        raise NotImplementedError


    def get_subvector(self, x, i):
        # Backend dependent
        raise NotImplementedError
    

class GenericFieldSplitCouple(object):
    def __init__(self, split0, split1 = None):
        if not split1: 
            # square splitting
            split1 = split0
        
        if not isinstance(split0, GenericFieldSplit) and split0.__class__ == split1.__class__:
            err_msg = "Requires one or two fieldsplits of the same type."
            raise TypeError(err_msg)

        self.__splits = split0, split1

    def __getitem__(self, i):
        if not i in [0, 1]:
            err_msg = "Indices must be 0 or 1."
            raise IndexError(err_msg)
        return self.__splits[i]

    def get_sizes(self):
        return (self[0].get_sizes(), self[1].get_sizes())

    def get_submatrix(self, A, (i,j)):
        # Backend dependent
        raise NotImplementedError

            
class PETScFieldSplit(GenericFieldSplit):
    def __init__(self, subspaces):
        from petsc4py.PETSc import IS, COMM_WORLD

        if hasattr(subspaces, "__iter__") and all([isinstance(sub, IS) for sub in subspaces]):
            self.__petsc_is = subspaces
            GenericFieldSplit.__init__(self, [sub.array for sub in subspaces])
        else:
            GenericFieldSplit.__init__(self, subspaces)
            self.__petsc_is = [IS().createGeneral(self.dofs(i), comm = COMM_WORLD) for i in xrange(len(self))]

        
    def get_is(self, i):
        return self.__petsc_is[i]


    def get_isets(self):
        return self.__petsc_is


    def get_subvector(self, x, i):
        from dolfin import PETScVector, as_backend_type
        x_vec = as_backend_type(x).vec()
        
        if not self.get_sizes()[1] == x_vec.getSizes()[1]:
            err_msg = "Vector has incorrect length: expected {}, found {}".format(self.get_sizes()[1],
                                                                                  x_vec.getSizes()[1])
            raise ValueError
        
        xi_vec = x_vec.getSubVector(self.get_is(i))
        return PETScVector(xi_vec)


    def set_subvector(self, x, i, val):
        from dolfin import GenericVector, as_backend_type
        
        xi = self.get_subvector(x, i)

        if isinstance(val, GenericVector):
            # Make sure the vectors have compatible size (PETSc doesn't)
            if val.local_size() != self.get_block_sizes(i)[0]:
                err_msg = "Vector has incompatible local_size: expected {}, found {} "
                err_msg = err_msg.format(val.local_size(), self.get_block_sizes(i)[0])
                raise ValueError(err_msg)

        xi[:] = val
        as_backend_type(x).vec().restoreSubVector(self.get_is(i), xi.vec())


    def __create_vec(self):
        # creates vector without wrapping
        from petsc4py.PETSc import Vec, COMM_WORLD
        
        if COMM_WORLD.size > 1:
            # TODO: Get the ghosting from the subspaces
            vec = Vec().createGhost(-1, self.get_sizes(), comm = COMM_WORLD)
        else:
            vec = Vec().createGhost(0, self.get_sizes(), comm = COMM_WORLD)
        
        return vec


    def create_vector(self):
        vec = self.__create_vec()
        from blockpc import PETScBlockVector
        return PETScBlockVector(self, vec)


class PETScFieldSplitCouple(GenericFieldSplitCouple):
    def __init__(self, split0, split1 = None):
        if not isinstance(split0, PETScFieldSplit):
            split0 = PETScFieldSplit(split0)

        if not split1: split1 = split0
        
        elif not isinstance(split1, PETScFieldSplit):
            split1 = PETScFieldSplit(split1)

        GenericFieldSplitCouple.__init__(self, split0, split1)


    def get_submatrix(self, A, (i,j)):
        from dolfin import as_backend_type, PETScMatrix
        Aij_mat = as_backend_type(A).mat().getSubMatrix(self[0].get_is(i), 
                                                        self[1].get_is(j))
        return PETScMatrix(Aij_mat)
        
        
        
        
        
    
