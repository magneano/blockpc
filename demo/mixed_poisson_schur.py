"""This test solves the mixed poisson problem using gmres
preconditioned with an eact Schur factorisation 

 inv(A) = L * B * R

The gmres solver should converge in 1 to 3 iterations depending on the
precondtioner P as follows:

     P     | # its
 ----------------------
     B     |  3 
   L * B   |  2 
   B * R   |  2  
 L * B * R |  1 



Because the the number of iterations needed are known this test is
useful for verifying the block multiplication is carried out correctly.


"""


from dolfin import *
from blockpc import *
import petsc4py
from petsc4py import PETSc

mesh = UnitSquareMesh(16, 16)
V = FunctionSpace(mesh, "CG", 2)
Q = FunctionSpace(mesh, "DRT", 1)
W = Q * V

bc = [DirichletBC(W.sub(1), 0., "on_boundary")]

p, u = TrialFunctions(W)
q, v = TestFunctions(W)
wh = Function(W); ph, uh = wh.split()


a = (inner(p, q) - inner(grad(u), q) - inner(grad(v), p) ) * dx
L = - v * dx

# assemble 
A, b = block_assemble_system(a, L, bc)

# build Schur complement
A00inv = LU(A[0,0], pc_factor_mat_solver_package = 'petsc').collapse()
S = A[1,1] - A[1,0] * A00inv * A[0,1]

Sinv = LU(S, pc_factor_mat_solver_package = 'petsc')


# set up the preconditioner; full schur factorisation
B = PETScBlockOperator(W)
B[:, :] = [[ A00inv,    0 ],
           [      0, Sinv ]]

L = PETScBlockOperator(W)
L[:, :] = [[ 1, - A00inv * A[0,1] ],
           [ 0,                 1 ]]

R = PETScBlockOperator(W)
R[:, :] = [[                1,  0 ],
           [ -A[1,0] * A00inv,  1 ]]

# randomize initial guess
import numpy
x = wh.vector()
x[:] = numpy.random.randn(x.local_size())

# solve
def solve(P):
    solver = PETScKrylovSolver("gmres")
    solver.parameters["monitor_convergence"] = True
    solver.parameters["nonzero_initial_guess"] = True
    solver.set_operator(A)
    solver.set_preconditioner(P)
    solver.solve(Vector(x), b)
    return x
    
print "Solve with P = B"
solve(B)

print "\nSolve with P = L*B"
solve(L*B)

print "\nSolve with P = B*R"
solve(B*R)

print "\nSolve with P = L*B*R"
solve(L*B*R)
