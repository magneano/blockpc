from dolfin import *
from blockpc import *

mesh = UnitSquareMesh(7, 7)
V = FunctionSpace(mesh, "CG", 1)
Q = FunctionSpace(mesh, "DG", 0)

V3 = VectorFunctionSpace(mesh, "CG", 1, dim = 3)
VQ = MixedFunctionSpace([V, Q])

spaces = [V, Q, V3, VQ]

vectors = v,q, v3, vq \
        = [as_backend_type(Function(space).vector()) for space in spaces]

w = vq.copy()

A = assemble(inner(TrialFunction(VQ), TestFunction(VQ)) * dx)

### ---- PETScFieldSplit tests ----
def test_PETScFieldSplit_init():
    # check that initialization succeeds for valid input
    split0 = PETScFieldSplit(V)
    split1 = PETScFieldSplit(V3)
    split2 = PETScFieldSplit(VQ)
    split3 = PETScFieldSplit([V,Q])

    # check that the number of blocks is correct
    assert len(split0) == 1
    assert len(split1) == 3
    assert len(split2) == 2
    assert len(split3) == 2

    # check (global) dimensions
    assert split0.get_sizes()[1] == split0.get_block_sizes(0)[1] == V.dim()
    
    assert split1.get_sizes()[1] == V3.dim()
    assert split1.get_block_sizes(0)[1] == V.dim()
    assert split1.get_block_sizes(1)[1] == V.dim()
    assert split1.get_block_sizes(2)[1] == V.dim()

    assert split2.get_sizes()[1] == VQ.dim()
    assert split2.get_block_sizes(0)[1] == V.dim()
    assert split2.get_block_sizes(1)[1] == Q.dim()

    assert split3.get_sizes()[1] == V.dim() + Q.dim()
    assert split3.get_block_sizes(0)[1] == V.dim()
    assert split3.get_block_sizes(1)[1] == Q.dim()


def test_PETScFieldSplit_create_vector():
    split0 = PETScFieldSplit(V)
    split1 = PETScFieldSplit(V3)
    split2 = PETScFieldSplit(VQ)
    split3 = PETScFieldSplit([V,Q])
    
    # check vector creation succeeding
    _v = split0.create_vector()
    _v3 = split1.create_vector()
    _vq = split2.create_vector()
    _w = split3.create_vector()
    
    # check that all vectors are block vectors
    assert all(isinstance(vec, PETScBlockVector) for vec in [_v, _v3, _vq, _w])

    # check that sizes are the correct
    assert _v.vec().getSizes() == v.vec().getSizes()
    assert _v3.vec().getSizes() == v3.vec().getSizes()
    assert _vq.vec().getSizes() == vq.vec().getSizes()
    assert _w.vec().getSizes() == (v.vec().getSizes()[0] + q.vec().getSizes()[0],
                                   v.vec().getSizes()[1] + q.vec().getSizes()[1])

    # assign values
    from numpy.random import rand
    _v.set_local(rand(_v.local_size()))

def test_PETScFieldSplit_get_subvector():
    split0 = PETScFieldSplit(V)
    split1 = PETScFieldSplit(V3)
    split2 = PETScFieldSplit(VQ)
    split3 = PETScFieldSplit([V,Q])
    
    # check subvector access succeeds
    v0 = split0.get_subvector(v, 0)
    
    v30 = split1.get_subvector(v3, 0)
    v31 = split1.get_subvector(v3, 1)
    v32 = split1.get_subvector(v3, 2)

    vq0 = split2.get_subvector(vq, 0)
    vq1 = split2.get_subvector(vq, 1)
    
    w0 = split3.get_subvector(w, 0)
    w1 = split3.get_subvector(w, 1)
    
    # check consistency of (local and global) dimensions
    assert v0.vec().getSizes() == split0.get_block_sizes(0)

    assert v30.vec().getSizes() == split1.get_block_sizes(0)
    assert v31.vec().getSizes() == split1.get_block_sizes(1)
    assert v32.vec().getSizes() == split1.get_block_sizes(2)

    assert vq0.vec().getSizes() == split2.get_block_sizes(0)
    assert vq1.vec().getSizes() == split2.get_block_sizes(1)

    assert w0.vec().getSizes() == split3.get_block_sizes(0)
    assert w1.vec().getSizes() == split3.get_block_sizes(1)
    
    # check that invalid access is caught: Invalid index
    try:
        v33 = split1.get_subvector(v3, 3)
        raise AssertionError
    except IndexError:
        pass

    try:
        vq2 = split2.get_subvector(vq, 2)
        raise AssertionError
    except IndexError:
        pass

    # check that invalid access is caught: Invalid vector
    try:
        t = split1.get_subvector(v, 0)
        raise AssertionError
    except ValueError:
        pass


def test_PETScFieldSplit_set_subvector():
    split0 = PETScFieldSplit(V)
    split1 = PETScFieldSplit(V3)
    split2 = PETScFieldSplit(VQ)
    split3 = PETScFieldSplit([V,Q])

    # setting subvectors to constant
    split0.set_subvector(v, 0, 1)
    
    split1.set_subvector(v3, 0, 1)
    split1.set_subvector(v3, 1, 2)
    split1.set_subvector(v3, 2, 3)
    
    split2.set_subvector(vq, 0, 1)
    split2.set_subvector(vq, 1, 2)

    split3.set_subvector(w, 0, 1)
    split3.set_subvector(w, 1, 2)

    # check values
    assert all(split0.get_subvector(v, 0) == 1)

    assert all(split1.get_subvector(v3, 0) == 1)
    assert all(split1.get_subvector(v3, 1) == 2)
    assert all(split1.get_subvector(v3, 2) == 3)

    assert all(split2.get_subvector(vq, 0) == 1)
    assert all(split2.get_subvector(vq, 1) == 2)

    assert all(split3.get_subvector(w, 0) == 1)
    assert all(split3.get_subvector(w, 1) == 2)

    # check that invalid access is caught:
    try:
        split1.set_subvector(vq, 0, 1.)
        raise AssertionError
    except ValueError:
        pass
    try:
        split2.set_subvector(v3, 1, 2.)
        raise AssertionError
    except ValueError:
        pass

    # check vector assignment
    v0 = split0.get_subvector(v, 0).copy()
    v0[:] = -1.
    split0.set_subvector(v, 0, v0)

    v30 = split1.get_subvector(v3, 0).copy()
    v30[:] = -1.
    split1.set_subvector(v3, 0, v30)
    v31 = split1.get_subvector(v3, 1).copy()
    v31[:] = -2.
    split1.set_subvector(v3, 1, v31)
    v32 = split1.get_subvector(v3, 2).copy()
    v32[:] = -3.
    split1.set_subvector(v3, 2, v32)

    vq0 = split2.get_subvector(vq, 0).copy()
    vq0[:] = -1.
    split2.set_subvector(vq, 0, vq0)
    vq1 = split2.get_subvector(vq, 1).copy()
    vq1[:] = -2.
    split2.set_subvector(vq, 1, vq1)

    w0 = split3.get_subvector(w, 0).copy()
    w0[:] = -1.
    split3.set_subvector(w, 0, w0)
    w1 = split3.get_subvector(vq, 1).copy()
    w1[:] = -2.
    split3.set_subvector(w, 1, w1)

    # check values
    assert all(split0.get_subvector(v, 0) == -1)

    assert all(split1.get_subvector(v3, 0) == -1)
    assert all(split1.get_subvector(v3, 1) == -2)
    assert all(split1.get_subvector(v3, 2) == -3)

    assert all(split2.get_subvector(vq, 0) == -1)
    assert all(split2.get_subvector(vq, 1) == -2)

    assert all(split3.get_subvector(w, 0) == -1)
    assert all(split3.get_subvector(w, 1) == -2)


    # try setting with invalid vector
    try:
        split2.set_subvector(v3, 1, vq1)
        raise AssertionError
    except ValueError:
        pass


### ---- PETScFieldSplitCouple tests ----
def test_PETScFieldSplitCouple_init():
    split0 = PETScFieldSplitCouple(PETScFieldSplit(VQ))
    split1 = PETScFieldSplitCouple(VQ)
    split2 = PETScFieldSplitCouple(VQ, VQ)
    split2 = PETScFieldSplitCouple([V,Q])

def test_PETScFieldSplitCouple_get_submatrix():
    split0 = PETScFieldSplitCouple(PETScFieldSplit(VQ))
    split1 = PETScFieldSplitCouple(VQ)
    split2 = PETScFieldSplitCouple(VQ, VQ)

    A00_0 = split0.get_submatrix(A, (0,0))
    A00_1 = split1.get_submatrix(A, (0,0))
    A00_2 = split2.get_submatrix(A, (0,0))

    assert (A00_0 - A00_1).norm("linf") < DOLFIN_EPS
    assert (A00_1 - A00_2).norm("linf") < DOLFIN_EPS

    A01 = split1.get_submatrix(A, (0,1))
    A11 = split1.get_submatrix(A, (1,1))

    assert A01.norm("linf") == 0
    
    split3 = PETScFieldSplitCouple([V,Q])
    B00 = split3.get_submatrix(A, (0,0))
    B01 = split3.get_submatrix(A, (0,1))
    B11 = split3.get_submatrix(A, (1,1))

    assert B01.norm("linf") > DOLFIN_EPS
