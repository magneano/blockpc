""" Solves the optimal control problem
 
   min  (u - d)**2 * ds + alpha * f**2 * dx

subject to the pde constraint

   - div grad u = f  in (0, 1) x (0, 1)
          du/dn = 0  on the boundary
"""


from dolfin import *
from blockpc import *
from matplotlib import pyplot
from numpy.random import randn, seed

parameters["plotting_backend"] = "matplotlib"

alpha = 1e-4
N     = 32

d = Expression("sin(pi*x[0])*pow(1+x[1], 2)")

mesh = UnitSquareMesh(N, N)
W = VectorFunctionSpace(mesh, "CG", 1, dim = 3)
V = FunctionSpace(mesh, "CG", 1)
w = Function(W)

J = Constant(alpha) * w[0]**2 * dx + (w[1] - d)**2 * ds \
  + inner(grad(w[1]), grad(w[2])) * dx + w[0]*w[2]*dx

rhs = -derivative(J, w, TestFunction(W))
lhs = -derivative(rhs, w, TrialFunction(W))

A = block_assemble(lhs)
b = block_assemble(rhs)

B = PETScBlockOperator(W)

# build preconditioner
M = A[2,0]
K = A[2,1]
O = A[1,1]

# get diagonal mass matrix approximation
D = assemble(TrialFunction(V) * TestFunction(V) * dx,
             form_compiler_parameters = {"quadrature_rule"  : "vertex",
                                         "quadrature_degree": 1})

B[0,0] = alpha**-1 * SOR(M, pc_sor_lits = 2)
B[1,1] = AMG(alpha * K * Jacobi(D) * K + O,
             pc_hypre_boomeramg_strong_threshold = 0.5,
             pc_hypre_boomeramg_relax_type_all = "symmetric-SOR/Jacobi", 
             pc_hypre_boomeramg_relax_weight_all = 1,
             pc_hypre_boomeramg_grid_sweeps_all = 1,
             pc_hypre_boomeramg_cycle_type = "W",
             pc_hypre_boomeramg_interp_type = "classical")

B[2,2] = alpha * SOR(M, pc_sor_lits = 2)

B = alpha * B

# solver setup
solver = PETScKrylovSolver("minres")
solver.parameters["monitor_convergence"] = True
solver.parameters["relative_tolerance"]  = 1e-8 
solver.parameters["maximum_iterations"]  = 200
solver.parameters["absolute_tolerance"]  = 1e-15
solver.parameters["divergence_limit"]  = 1e16
solver.parameters["error_on_nonconvergence"]  = False

solver.set_operator(A)
solver.set_preconditioner(B)

solver.solve(w.vector(), b)

pyplot.figure(0)
plot(w[0])
pyplot.title("optimal_control")

pyplot.figure(1)
plot(w[1])
pyplot.title("optimal_state")

pyplot.show()

